# 解决 ------- 捕获处理异常
# --------------- 方案一 ----------------------------------------------------------------
'''
    try:
        可能出现的异常代码  --  这里不管以后会抛出多少个异常，只会从上往下检测，检测到一个后，就立即往下去匹配，不会多次检测
    except 你要捕获一异常类别，as 接收异常的形参: --- 这一块可以有多个重复，用于捕捉可能的其他异常，如果针对多个不常有相同的处理方式，那么可以将多个异常合并
        对于这个异常的处理
    else:
        没有出现异常时做的处理 -- 这一块必须放在上一块except结束后(可以省略)
    finally:
        不管有没有出现异常，都会执行的代码

注意:
    try 语句没有捕捉到异常，先执行try代码段后，在执行else，最后执行finally
    如果try捕获异常，首先执行except处理错误，然后执行finally
    若果异常名称不确定，而又想捕捉，可以直接写Exception
'''

# 例子：
try:
    1/0
    print(name)
# 方法一：
# except ZeroDivisionError:
#     print("XXX")
# except NameError:
#     print("名称异常")

# 方法二
# except (ZeroDivisionError, NameError):  # -- 将多个异常合并成一个异常
#     print("XXX")

# 方法三
except Exception as e:
    print("XXX", e)  # --- XXX division by zero
else:
    print("123")
finally:
    print("最后执行的内容，不管是否出现异常，都会执行语句")

# 运行结果：
# XXX
# 最后执行的内容，不管是否出现异常，都会执行语句


# ------------- 方案二 -------------------------------------
# 作用：适用于执行某一段代码A之前，进行预处理，执行代码A结束后，进行清理操作
#      不管出现了什么异常，最终都要执行一些清理操作

# 语法：
#   with context_expression [as target(s)]:   -- 详情讲解，请打开异常处理 -- with context_expression.png
#       with body

# -- 示例
# 文件读取操作正常步骤：
#   1，打开文件 -- f = open("XXX.txt")
#   2，读取文件 -- f.readlines()  -- 如果这一行代码抛出异常，就会影响后续“文件关闭“操作的执行
#   3，关闭文件 -- f.close()

# 想保证， -- 不管读取文件文件操作有没有异常， 都要关闭文件：
#   try:
#       打开文件， 读取文件
#   finally:
#       关闭文件

# 但是上面写法过于繁琐，于是有了这个方法：
#   with open(file) as f:  -- f是这个文件的对象 -- with 包含了打开和关闭操作
#       读取文件

# 代码：
# with open("处理异常", "rb") as f:
#     f.readline()

# ---------------- 补充 --------------------------------
# 自定义上下文管理器
class Test:
    def __enter__(self):
        print("Enter Test")

    def __exit__(self,exc_type, exc_val, exc_tb):
        print(self, exc_type,exc_val,exc_tb)
        print("exit")

with Test():
    print("body")

# 运行结果：

# Enter Test
# body
# <__main__.Test object at 0x000001A7B6F18DF0> None None None
# exit


class Test:
    def __enter__(self):
        print("Enter Test")

    def __exit__(self,exc_type, exc_val, exc_tb):
        print(self, exc_type,exc_val,exc_tb)
        print("exit")

with Test() as s:
    print("body",s)

# 运行结果：
# Enter Test
# body None  # 这里None 表示的是enter函数执行的返回值，这里None表示没有返回值
# <__main__.Test object at 0x000001F107129FA0> None None None
# exit


class Test:
    def __enter__(self):
        print("Enter Test")
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        print(self, exc_type, exc_val, exc_tb)  # exc_tb 表示追踪信息: 错误文件、错误行、错误所含的模块
        print("exit")

        # 程序不崩溃的情况下，追踪到错误信息
        import traceback
        print(traceback.extract_tb(exc_tb))  # [<FrameSummary file D:\workspace\Python基础\pyhon -- 异常处理\常见的系统异常.py, line 147 in <module>>]

        return True  # -- True 作用：产生的异常不会被抛出，自己内部消化，即程序不会报错


with Test() as s:
    print("body", s)
    1/0



# 运行结果：
# Enter Test
# body <__main__.Test object at 0x000001E38F7AAAF0>
# <__main__.Test object at 0x000001E38F7AAAF0> None None None
# exit



# --------------------------- contextlib模块------------------------------------------------
# @contextlib.contextmanager -- 使用装饰器，让一个生成器变成一个“上下文管理器”

import contextlib

@contextlib.contextmanager
def tst1():
    print(1)
    yield "xxx"
    # yield 上半部分相当于执行的是 enter函数 ， 下半部分执行的是 exist函数， yield 后面的“xxx”表示的是enter执行的返回值
    print(2)

with tst1() as s:
    print(3, s)

# 运行结果：
# 1
# 3 xxx
# 2


# 实例：
@contextlib.contextmanager
def tst2():
    try:
        yield
    except ZeroDivisionError as e:
        print("error:",e)

x = 1
y = 0
with tst2():
    x/y

# 运行结果： error: division by zero


# contextlib.closing -- 这个函数，让一个拥有close方法但不是上下文管理器的对象变成”上下文管理器“
class Test:
    def t(self):
        print("ttttttt")

    def close(self):
        print("资源释放")


import contextlib

with contextlib.closing(Test()) as t_obj:
    t_obj.t()

# 运行结果：
# ttttttt
# 资源释放


# #  ------  多个上下文管理器的嵌套
# with open("xxx.png", "rb") as from_file:
#     with open("sss.png", "wb") as to_file:
#         from_contests = from_file.read()
#         to_file.write(from_contests)


# python 3.7 改进
with open("xxx.png", "rb") as from_file, open("sss.png","wb") as to_file:
    from_contests = from_file.read()
    to_file.write(from_contests)
