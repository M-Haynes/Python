# 除0异常 -- ZeroDivisionError
# 名称异常 -- NameError
# 类型异常 -- TypeError
# 索引异常 -- IndexError
# 键异常   -- KeyError
# 值异常   -- ValueError
# 属性异常  -- AttributeError
# 迭代器异常 -- StopIterationError

# 系统异常类继承树 -- BaseException 所有内建的异常的基类：
#                  SystemExit -- 由sys.exit()函数引发，当它处不处理是，Python解释器退出
#                  KeyboardInterrupt -- 当用户点击中断键(通常 ctrl + c) 时引发
#                  GeneratorExit -- 当调用一种generator的close()方法时引发
#                  Exception -- 所有内置的，非系统对出异常是从该类派生的，应该从该类派生所有用户定义的异常





