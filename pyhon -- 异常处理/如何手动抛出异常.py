# 通过raise语句直接抛出相关类型的异常
# 什么时候应该向外界抛出异常？ 什么时候应该内部做容错处理？

# -- 例子：
def set_age(age):
    if age <= 0 and age >= 150:
        # print("值错误")
        raise ValueError("值错误")  # -- 直接报出错误数据
    else:
        print("给张三的年龄设置为", age)


try:
    set_age(-18)

except ValueError as e:
    print("X", e)
