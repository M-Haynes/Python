# 给定一批文件（不同类型）
# 按照文件后缀名，划分到不同的文件夹中
#   文件夹命名:后缀名
#   结果： avi -- a.avi、 e.avi
#         jpg -- b.jpg、 c.jpg



import os
import shutil # 移动文件模块


# 增加容错处理
path = "文件1"
if not os.path.exists(path):
    exit()

os.chdir("文件1")  # 切换目录
# file_list = os.listdir("文件1")  # 方式一  -- 这里是路径没有切换到目录文件
file_list = os.listdir("./") # 方式二 -- 在已经将路径切换到目标目录后
print(file_list)  # ['a.avi', 'a.txt', 'bb.text', 'bbb', 'bbb_附件.txt', 'bb_fujian.text', 'xxx.png', 'xxx2.png']

# 1 遍历所有的文件（名称）
for file_name in file_list:
    #  2 分解文件的后缀名
    ###   方法一 -- 用字符串切片的方式
    # 获取点的位置
    id = file_name.rfind(".")  # 返回的是index
    if id == -1:  # 这里做一个容错处理
        continue
    # 根据位置拿取后缀字符串
    extension = file_name[id+1::]
    # print(extension)

    ### 方法二 -- 直接使用split函数选取
    extension2 = file_name.split(".")[-1]
    print(extension2)  # 直接使用split函数选取

#  3 查看一下，是否存在同名的目录
#  4 如果不存在这样的目录，-> 直接创建一个这样名称的目录
#  5  目录存在 --> 移动过去
    if not os.path.exists(extension):
        # 创建
        os.mkdir(extension)
    # 移动
    shutil.move(file_name, extension)

