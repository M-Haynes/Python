# ----- 定位 -----------
# f.seek(偏移量， [0,1,2])
#   0 -- 开头 -- 默认
#   1 -- 当前位置 ， 偏移量可正可负
#   2 -- 文件末尾 ， 偏移量只能是负的
#   注意 文本文件的操作模式下（不带b） --  只能写0
#       若果想要写1/2， 必须要在二进制文件操作模式下（带b）

# f.tell() -- 用于查看文件指针位置的函数


f = open("文件1/a.txt", "rb")

print(f.tell())  # 这里是打印指针 -- 0
f.seek(-2, 2)  # 这里因为文件是以2进制来读取，所以这里的指针是在文件的末尾，偏移量是-1 表示往前移动两个
print(f.tell())  # 这里是打印指针 -- 4

print(f.read())  # -- b'\xc4\xe3'
print(f.tell())  # 6


# ----------------------------  文件的 读 操作 ----------------------------------------------------------------
f = open("文件1/a.txt", "r")
# f.read(字节数)  #
#   字节数默认是文件内容长度，下标会自动后移
'''
content = f.read()
print(content)  # 我和你
print("#"*10)

f.seek(2)
print(f.tell())  # 指针在第二个
content2 = f.read(2)  # 和你
print(content2)
'''

# f.readline([limit])
# 读取一行数据，limit -- 限制的最大字节数
'''
print("------", f.tell())  # ------ 0
content3 = f.readline()
print(content3, end="")  # 表示打印完毕时的结束符不要用换行，而是用空字符串  # 我和你

print("------", f.tell())  # ------ 8
content3 = f.readline()
print(content3, end="")  # 1112321
'''

# f.readlines()
#   会自动的将文件换行符进行处理，
#   将处理好的每一行组成一列表返回
#   读取的时候，直接通过for in 就可以读取到
'''
content4 = f.readlines()
print(content4)  # ['我和你\n', '1112321']
for i in content4:
    print(i,end="")
    
'''

# for in
#   可以直接遍历f本身 -- 拆解文件 也是按照行进行拆解的
#   也可以遍历列表
import collections
f = open("文件1/a.txt", "r")

print(isinstance(f,collections.Iterator)) # 是一个迭代器
for line in f:
    print(line,end="")
# 执行结果：
#   我和你
#   1112321
f.close()


# 判断是否可读
# f.readable()
#   为了避免文件不可读导致的报错
f = open("文件1/a.txt")
if f.readable():
    content = f.read()
    print(content)
print("$"*10)  # $$$$$$$$$$ -- 代码不会报错
# 注意
#     一般文件特别大的时候，可以使用readline ， 或 for in 方法
#           按行加载，可节省內存,但是相比于其他两个读取方法，性能较低
#     其他两个方法(f.read() 、 f.readlines())，一次性读取文件所有内容
#           虽然占用內存，但处理性能比较高


# ---------------------------------------文件的 写 操作 --------------------------------
# f.write("内容")
#   返回的是写入的字节长度
# 判断是否可写 -- writeable()


f = open("文件1/a.txt", "a")

if f.writable():
    print(f.write("我爱你"))  # 3 返回写入的字节数

f.close()


# --------------------------------------- 文件的 关闭 操作 --------------------------------
f.close()
#   关闭一个打开的文件

f.flush()
# 立即清空缓冲区的数据内容到磁盘文件里