# 模块 import os
# 操作：

import os
#   重命名
os.rename("b.text", "文件1/bb.text")

# 改目录和目录下的文件
os.renames("文件/aaa", "文件1/bbb") # 这里同时将目录文件和目录下的文件 的名字都改变了

#   删除
#   删除文件 -- os.remove("文件路径")  -- 注意 文件不存在，则会报错
#   删除目录
#       os.rmdir(path) -- 不能递归删除目录，如果文件夹非空，会报错
#       os.removedirs(path) -- 可以递归的删除目录,如果文件夹非空，会报错

os.rmdir("文件1/a.txt")  # 非空 会报错

os.removedirs("文件1/bbb") # 这里会从bbb文件开始删除，一直到删除完整个路径的文件，即文件1被删除


# 创建文件夹
#   os.mkdir("文件夹名称",[,model]) -- 不能递归创建，即只能创建一级目录文件，不能创建多级目录文件
os.mkdir("aa")


# 获取当前目录
os.getcwd()
print(os.getcwd())  # D:\code\Python基础\Python 文件操作

# 改变默认目录
os.chdir("文件1")
print(os.getcwd())  # D:\code\Python基础\Python 文件操作\文件1 -- 将文件目录切换成文件1

# 获取目录内容列表
# os.listdir("./")
# ./ -- 表示当前所在目录 -- 且只是表示一级目录
# ../ -- 表示当前所在目录的上一级目录

print(os.listdir("文件1"))  # ['bbb']