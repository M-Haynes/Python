# 给定一批文件
#   1，按照文件后缀名，划分不同的文件、
#   2，生成.txt格式的文件清单

# 方法 -- 使用递归
import os
file_list = os.listdir("文件1")
# print(file_list)

# 通过给定的文件夹，列举出这个文件夹当中，所有的文件，以及文件夹，子文件夹当中的所有文件
def listFiletotext(dir,file):
    pass
    # 1.列举出，当前给定的文件夹，下的所有子文件夹，已经子文件
    file_list = os.listdir(dir)
    # 2. 针对子，列举的列表，进行遍历
    for file_name in file_list:
        new_filename = dir + "/" + file_name  # 注意-- 这里的路径判断是相对的，
        # 判定，是否是目录，是目录，再次调用listFile文件
        if os.path.isdir(new_filename):
            print(new_filename)
            file.write(new_filename+"\n")
            listFiletotext(new_filename,file)
        else:
            # 打印下，文件名称
            # print("\t"+ file_name)  # "\t" 作用是增加空格
            file.write("\t" + new_filename + "\n")
    # print("")
    file.write("\n")
f = open("list.txt","a") # 这里创建并打开一个文件夹
listFiletotext("文件1",f)
