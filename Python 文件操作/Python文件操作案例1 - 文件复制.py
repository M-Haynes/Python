# 文件的复制
# 要求 -- 讲一个文件，复制到另外一个副本中
# 步骤

# 切换文件目录
import os
os.chdir("文件1")  # 切换文件路劲
print(os.getcwd())

f_b = open("bbb", "r", encoding="utf_8")
f_copy = open("bbb_附件.txt", "a", encoding="utf-8")
content = f_b.read()
print(content)
f_copy.write(content)

f_b.close()
f_copy.close()

# # 1 只读模式，打开要复制的文件 -- 追加模式，打开副本
# f = open("bb.text","r", encoding="UTF_8")  # 采用是是UTF-8的形式
# f_1 = open("bb_fujian.text", "a", encoding= "UTF_8")  # 以追加的形式打开副本
#
# # 2 从源文件中读取内容 -- 写入到目标文件中
# original_content = f.read()
# f_1.write(original_content)
#
# # 3 关闭源文件和目标文件
#
# f.close()
# f_1.close()



# 当大文件的时候，需要分步骤一步一步取读取 --用 for循环
while True:
    content = f_b.read(1024)
    if len(content) == 0:
        break
    f_copy.write(content)
