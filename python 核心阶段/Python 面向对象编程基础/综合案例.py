# 计算器，实现一届基本的操作，加减乘除运算，以及打印结果操作

# -------------------------------------------- 代码一 ---------------------------------
# class Caculator:
#     __result = 0
#
#     @classmethod
#     def first_value(cls,v):
#         cls.__result = v
#
#     @classmethod
#     def jia(cls, n):
#         cls.__result += n
#
#     @classmethod
#     def jian(cls, n):
#         cls.__result -= n
#
#     @classmethod
#     def cheng(cls, n):
#         cls.__result *= n
#
#     @classmethod
#     def show(cls):
#         print("结果是：%s"%cls.__result)
#
# Caculator.first_value(2)
# Caculator.jia(6)
# Caculator.jian(4)
# Caculator.cheng(5)
# Caculator.show()
# 结果是：20


# ---------------------------------------- 代码二  ------------------------------------------
class Caculator:

    def __init__(self,num):
        self.__result = num

    def jia(self, n):
        self.__result += n

    def jian(self, n):
        self.__result -= n

    def cheng(self, n):
        self.__result *= n

    def show(self):
        print("结果是：%s"% self.__result)

# c1 = Caculator(2)
#
# c1.jia(6)
# c1.jian(4)
# c1.cheng(5)
# c1.show()
# # 结果是：20


# ---------------------------------------- 代码三   ------------------------------------------
# 增加容错处理
class Caculator:

    def check_num(self, num):
        if not isinstance(num,int):
            raise TypeError("当前这个数据类型有问题，应该是一个整型数据")

    def __init__(self,num):
        self.check_num(num)
        # if not isinstance(num,int):
            # raise TypeError("当前这个数据类型有问题，应该是一个整型数据")
        self.__result = num

    def jia(self, n):
        self.check_num(n)
        # if not isinstance(n, int):
        #     raise TypeError("当前这个数据类型有问题，应该是一个整型数据")
        self.__result += n

    def jian(self, n):
        self.check_num(n)
        # if not isinstance(n,int):
        #     raise TypeError("当前这个数据类型有问题，应该是一个整型数据")
        self.__result -= n

    def cheng(self, n):
        self.check_num(n)
        # if not isinstance(n,int):
        #     raise TypeError("当前这个数据类型有问题，应该是一个整型数据")
        self.__result *= n

    def show(self):
        print("结果是：%s"% self.__result)

c1 = Caculator(2)

# c1.jia(6)
# c1.jian("cac")
# c1.cheng(5)
# c1.show()
# 运行结果：TypeError: 当前这个数据类型有问题，应该是一个整型数据


# ------------------------------------- 代码四 ------------------------------
# 增加容错处理 -- 使用装饰器
class Caculator:

    # 设置成私有的装饰器，让外界不能调用
    def __check_num_zsq(func):
        def inner(self,n):
            if not isinstance(n,int):
                raise TypeError("当前这个数据类型有问题，应该是一个整型数据")
            return func(self,n)
        return inner

    @__check_num_zsq
    def __init__(self,num):
        self.__result = num

    @__check_num_zsq
    def jia(self, n):
        self.__result += n

    @__check_num_zsq
    def jian(self, n):
        self.__result -= n

    @__check_num_zsq
    def cheng(self, n):
        self.__result *= n

    def show(self):
        print("结果是：%s"% self.__result)

# c1 = Caculator(2)
# c1.jia(6)
# c1.jian(4)
# c1.cheng(5)
# c1.show()


# ------------------------------------- 代码五 ------------------------------
# 增加一个播报器

import win32com.client

# 1,创建一个播报对象
speaker = win32com.client.Dispatch("SAPI.SpVoice")

# 2，通过这个播报器对象，直接，播放相对应的语音字符串就可以
speaker.Speak("我的名字是sz")

class Caculator:

    def __say(self,word):
        # 1,创建一个播报对象
        speaker = win32com.client.Dispatch("SAPI.SpVoice")
        # 2，通过这个播报器对象，直接，播放相对应的语音字符串就可以
        speaker.Speak(word)

    # 设置成私有的装饰器，让外界不能调用
    def __check_num_zsq(func):
        def inner(self,n):
            if not isinstance(n,int):
                raise TypeError("当前这个数据类型有问题，应该是一个整型数据")
            return func(self,n)
        return inner

    @__check_num_zsq
    def __init__(self,num):
        self.__say(num)
        self.__result = num

    @__check_num_zsq
    def jia(self,n):
        self.__say(n)
        self.__result += n

    @__check_num_zsq
    def jian(self,n):
        self.__say(n)
        self.__result -= n

    @__check_num_zsq
    def cheng(self, n):
        self.__say(n)
        self.__result *= n

    def show(self):
        self.__say("计算的结果是：%d"%self.__result)
        print("结果是：%s"% self.__result)

# c1 = Caculator(2)
# c1.jia(6)
# c1.jian(2)
# c1.cheng(5)
# c1.show()


# ------------------------------------- 代码六 ------------------------------
# 增加一个播报器 -- 使用装饰器

import win32com.client


class Caculator:

    # 因为要实现对应计算的播放，__say_zaq(fync) 是不能传入两个参数的，
    # 所以需要在装饰器外面在设置一个装饰器，通过传入的参数创建一个对应的装饰器
    def create_say_zsq(word = ""):
        # 设置一个播报的装饰器
        def __say_zsq(func):
            def inner(self,n):
                # 1,创建一个播报对象
                speaker = win32com.client.Dispatch("SAPI.SpVoice")
                # 2，通过这个播报器对象，直接，播放相对应的语音字符串就可以
                speaker.Speak(word + str(n))
                return func(self,n)
            return inner
        return __say_zsq

    # 设置成私有的装饰器，让外界不能调用
    def __check_num_zsq(func):
        def inner(self,n):
            if not isinstance(n,int):
                raise TypeError("当前这个数据类型有问题，应该是一个整型数据")
            return func(self,n)
        return inner

    @__check_num_zsq
    @create_say_zsq()
    def __init__(self,num):
        self.__result = num

    @__check_num_zsq
    @create_say_zsq("加")
    def jia(self,n):
        self.__result += n

    @__check_num_zsq
    @create_say_zsq("减")
    def jian(self,n):
        self.__result -= n

    @__check_num_zsq
    @create_say_zsq("乘")
    def cheng(self, n):
        self.__result *= n

    def show(self):
        print("结果是：%s"% self.__result)

# c1 = Caculator(2)
# c1.jia(6)
# c1.jian(2)
# c1.cheng(5)
# c1.show()


# ------------------------------------- 代码七 ------------------------------
# 增加一个播报器 -- 将播放器的内容提取出来，设置成一个内置方法，目的是用于后期的可维护性

import win32com.client


class Caculator:

    def __say(self, word):
        # 1,创建一个播报对象
        speaker = win32com.client.Dispatch("SAPI.SpVoice")
        # 2，通过这个播报器对象，直接，播放相对应的语音字符串就可以
        speaker.Speak(word)


    # 因为要实现对应计算的播放，__say_zaq(fync) 是不能传入两个参数的，
    # 所以需要在装饰器外面在设置一个装饰器，通过传入的参数创建一个对应的装饰器
    def __create_say_zsq(word = ""):
        # 设置一个播报的装饰器
        def __say_zsq(func):
            def inner(self,n):
                self.__say(word + str(n))
                return func(self,n)
            return inner
        return __say_zsq

    # 设置成私有的装饰器，让外界不能调用
    def __check_num_zsq(func):
        def inner(self,n):
            if not isinstance(n,int):
                raise TypeError("当前这个数据类型有问题，应该是一个整型数据")
            return func(self,n)
        return inner

    @__check_num_zsq
    @__create_say_zsq()
    def __init__(self,num):
        self.__result = num

    @__check_num_zsq
    @__create_say_zsq("加")
    def jia(self,n):
        self.__result += n

    @__check_num_zsq
    @__create_say_zsq("减")
    def jian(self,n):
        self.__result -= n

    @__check_num_zsq
    @__create_say_zsq("乘")
    def cheng(self, n):
        self.__result *= n

    def show(self):
        self.__say("计算结果是：%d" % self.__result)
        print("结果是：%s"% self.__result)

# c1 = Caculator(2)
# c1.jia(6)
# c1.jian(2)
# c1.cheng(5)
# c1.show()


# ------------------------------------- 代码八 ------------------------------
# 增加一个外部调用计算结果 -- 使用的描述器 --property

import win32com.client


class Caculator:

    def __say(self, word):
        # 1,创建一个播报对象
        speaker = win32com.client.Dispatch("SAPI.SpVoice")
        # 2，通过这个播报器对象，直接，播放相对应的语音字符串就可以
        speaker.Speak(word)


    # 因为要实现对应计算的播放，__say_zaq(fync) 是不能传入两个参数的，
    # 所以需要在装饰器外面在设置一个装饰器，通过传入的参数创建一个对应的装饰器
    def __create_say_zsq(word = ""):
        # 设置一个播报的装饰器
        def __say_zsq(func):
            def inner(self,n):
                self.__say(word + str(n))
                return func(self,n)
            return inner
        return __say_zsq

    # 设置成私有的装饰器，让外界不能调用
    def __check_num_zsq(func):
        def inner(self,n):
            if not isinstance(n,int):
                raise TypeError("当前这个数据类型有问题，应该是一个整型数据")
            return func(self,n)
        return inner

    @__check_num_zsq
    @__create_say_zsq()
    def __init__(self,num):
        self.__result = num

    @__check_num_zsq
    @__create_say_zsq("加")
    def jia(self,n):
        self.__result += n

    @__check_num_zsq
    @__create_say_zsq("减")
    def jian(self,n):
        self.__result -= n

    @__check_num_zsq
    @__create_say_zsq("乘")
    def cheng(self, n):
        self.__result *= n

    def show(self):
        self.__say("计算结果是：%d" % self.__result)
        print("结果是：%s"% self.__result)

    @property
    def result(self):  # 这个描述器，外界 只有读，没有写
        return self.__result

c1 = Caculator(2)
c1.jia(6)
c1.jian(2)
c1.cheng(5)
c1.show()
print(c1.result)


# ------------------------------------- 代码九 ------------------------------
# 增加一个清零操作，同时对实例的调用采链式的方式（使用完方式，返回的结果还是实例）

import win32com.client


class Caculator:

    def __say(self, word):
        # 1,创建一个播报对象
        speaker = win32com.client.Dispatch("SAPI.SpVoice")
        # 2，通过这个播报器对象，直接，播放相对应的语音字符串就可以
        speaker.Speak(word)


    # 因为要实现对应计算的播放，__say_zaq(fync) 是不能传入两个参数的，
    # 所以需要在装饰器外面在设置一个装饰器，通过传入的参数创建一个对应的装饰器
    def __create_say_zsq(word = ""):
        # 设置一个播报的装饰器
        def __say_zsq(func):
            def inner(self,n):
                self.__say(word + str(n))
                return func(self,n)
            return inner
        return __say_zsq

    # 设置成私有的装饰器，让外界不能调用
    def __check_num_zsq(func):
        def inner(self,n):
            if not isinstance(n,int):
                raise TypeError("当前这个数据类型有问题，应该是一个整型数据")
            return func(self,n)
        return inner

    @__check_num_zsq
    @__create_say_zsq()
    def __init__(self,num):
        self.__result = num

    @__check_num_zsq
    @__create_say_zsq("加")
    def jia(self,n):
        self.__result += n
        return self

    @__check_num_zsq
    @__create_say_zsq("减")
    def jian(self,n):
        self.__result -= n
        return self

    @__check_num_zsq
    @__create_say_zsq("乘")
    def cheng(self, n):
        self.__result *= n
        return self

    def show(self):
        self.__say("计算结果是：%d" % self.__result)
        print("结果是：%s"% self.__result)
        return self

    def clear(self):
        self.__result = 0
        return self

    @property
    def result(self):  # 这个描述器，外界 只有读，没有写
        return self.__result

c1 = Caculator(2)
c1.jia(6).jian(2).cheng(5).show().clear().jia(550).jian(300)
print(c1.result)