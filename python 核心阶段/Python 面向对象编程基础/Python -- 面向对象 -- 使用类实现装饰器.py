# ----------------------------------使用类 ，实现装饰器 ------------------------------
# 装饰器：
def check(func):
    def inner():
        print("登录验证")
        func()
    return inner

@check
def fashuoshuo():
    print("发说说")

# fashuoshuo()
# 运行结果：
# 登录验证
# 发说说


class Check:
    def __init__(self,func):
        self.f = func

    def __call__(self,*args,**kwargs):
        print("登录验证")
        self.f()


def fashuoshuo():
    print("发说说")

fashuoshuo = Check(fashuoshuo)

# fashuoshuo()
# 实行机制
# 在创建check类的实例时，将fashuoshuo这个函数作为参数，传入进类进行了初始化__init__，并保存了这个函数
# 在使用fashuoshuo() 表示调用了这个实例，这时候就调用__call__函数进行执行func这个函数

# ---或者：
class Check:
    def __init__(self,func):
        self.f = func

    def __call__(self,*args,**kwargs):
        print("登录验证")
        self.f()

@Check
def fashuoshuo():
    print("发说说")

fashuoshuo()
# 运行结果：
# 登录验证
# 发说说
