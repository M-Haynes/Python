# 概念 --- 生命周期
#   指的是一个对象,从诞生到消亡的过程
#   当一个对象被创建时,会在內存中分配相应的內存空间进行存储
#   当这个对象不在使用,为了节约的內存,就会把这个对象释放

# 涉及问题：
#   如何监听一个对象的生命过程？
#   python如何掌握一个对象的生命

# 监听对象生命周期：
#   __new__方法：
#       当我们创建一个对象时，用于给这个对象分配內存的方法
#       通过拦截这个方法，可以修改对象的创建过程 -- 单例设计模式
#   __init__方法：
#   __del__方法：

class Person:
    # 在创建一个类实例时，会先查找__new__函数并执行，然后类实例的进程终止
    # def __new__(cls,*args, **kwargs):
    #     print("新建一个对象，但是，被我拦截了")

    def __init__(self):
        print("初始化方法")
        self.name = "se"

    # 当一个实例执行完毕要被释放时，会自动去调用这个方法
    def __set__(self):
        print("这个对象被释放了")

p = Person()
print(p)
print(p.name)
del p  # 释放p

# 初始化方法
# <__main__.Person object at 0x000001F2D4943FD0>
# se


# ------------------------------------监听对象生命周期的方法 -- 小案例----------------------------

# Person，打印一下，当这个时刻，有Person类，产生的实例，有多少个？
# 创建了一个实例，计数 +1 ， 删除了一个实例 ，计数 —1

class Person:
    __personCount = 0  # 类属性
    def __init__(self):
        print("计数" + "1")
        Person.__personCount += 1

    def __del__(self):
        print("计数","-1")
        self.__class__.__personCount -= 1

    @staticmethod
    def log():
        print("当前的人的个数是%d个" % Person.__personCount)

p = Person()
p2 = Person()
Person.log()
del p
Person.log()
