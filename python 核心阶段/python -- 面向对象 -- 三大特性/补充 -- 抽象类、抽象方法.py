# --抽象类 == 作用，存储所有子类的一些共性  -- 只是抽象出来的概念，不能具体使用
#   一个抽象出来的类，并不是某一个具体化的类
#   不能直接创建实例的类，创建会报错

# --抽象方法 == 左右，存储所有子类的一个共有方法 -- 只是抽象出来的概念，不能具体使用
#   抽象出来的一个方法
#   不具备具体实现，不能直接调用， 子类不实现会报错

# -------------------------------Python中的实现---------------------------------
# 无法直接支持，需要借助一个模块 -- import abc
# 设置类的元类为 -- abc.ABCMeta
# 使用装饰器修饰抽象方法 -- @abc.abstractmethod

import abc
class Animal(object, metaclass= abc.ABCMeta):
    @abc.abstractmethod  # 设置抽象方法
    def jiao(self):
        pass

    @abc.abstractclassmethod  # 设置抽象类方法
    def tt(cls):
        pass

class Dog(Animal):

    def jiao(self):
        print("汪汪")

    @classmethod
    def tt(cls):
        print("XXX")



d = Dog()
d.jiao()
# 运行结果：汪汪