# 概念：
#   一个类"拥有"另外一个类的“资源”的方式之一
#   "拥有": 不是资源的复制，变成双份资源，而是字段的”使用权“
#   "资源"：指"非私有的"属性和方法
# 例子： Dog类继承自Animal类
import inspect


# 目的： 方便资源重用


# -------------------------------- 单继承 ----------------------------------------------------------------
# 概念：仅仅继承了一个父类
class Animal:
    pass

# 单继承
class Dog(Animal):
    pass





# -------------------------------- 多继承 ----------------------------------------------------------------
# 概念：继承了多个父类

class Animal:
    pass

class XXX:
    pass

# 多继承
class Dog(Animal, XXX):
    pass

# 查询继承的父类
print(Dog.__bases__)
# (<class '__main__.Animal'>, <class '__main__.XXX'>)

# 这里主要 类的继承是object 类， 和 type 类的联系和区别  ---- 见图片（type类 和 object 类 的区别）


# -----------------------------  继承下的影响 -------------------------------------------------

#  ------------ 资源的继承 --------------------------------
#   在python 中，继承是指 资源的使用权
#   所以在某个资源能否被继承，其实就是测试在子类当中，能不能访问到父类当中的这个资源

class Animal:
    # 属性 和方法
    # 设置不同权限的属性 和方法，继承当中，进行测试
    # 在子类当中，能够访问导到这些资源

    a = 1
    _b = 2
    __c = 3

    def t1(self):
        print("t1")

    # 受保护方法
    def _t2(self):
        print("t2")

    # 私有方法
    def __t3(self):
        print("t3")

    # 类中默认的实例方法
    def __init__(self):
        print("init, Animal")


# 设置一个子类
class Dog(Animal):
    def test(self):
        print(self.a)
        print(self._b)
        # print(self.__c)

        self.t1()
        self._t2()
        # self.__t3()
        self.__init__()


p = Dog()
p.test()
# 运行结论：  # 私有属性和 私有的方法是无法继承的

Animal.a = 666
print(Animal.a)
print(Dog.a)
# 666
# 666
# 表示在子类的中的资源是父类的，而不是自己重新复制了一份，但是子类的中的修改操作是新增的操作，不会进行修改父类中的数据

# 子类的赋值操作，不是修改父类的值，而是自己新建一个值
class A:
    age = 10

class B:
    pass


print(A.age)
B.age = 11
print(A.age)
# 10
# 10

print(A.__dict__)
print(B.__dict__)
# {'__module__': '__main__', 'age': 10, '__dict__': <attribute '__dict__' of 'A' objects>, '__weakref__': <attribute '__weakref__' of 'A' objects>, '__doc__': None}
# {'__module__': '__main__', 'age': 11,'__dict__': <attribute '__dict__' of 'B' objects>, '__weakref__': <attribute '__weakref__' of 'B' objects>, '__doc__': None}


# ------------ 资源的使用 --------------------------------
# 继承的几种形态
#   单继承链
#   无重叠的多继承链
#   有重叠的多继承链
#   ---- 详情见：（单继承 - 无重叠多继承 - 有重叠多继承）图


#  ------------- 几种形态应该遵循的标准原则 ------------------------
#  ---- ----  ---- Python2.2 版本

# MRO -- 方法解析顺序

#   深度优先 （从左往右）-- 沿着一个继承链，尽可能的往深了去找
#   具体算法步骤：
#       1,把根节点压入栈中，（栈 -- 先进后出）
#       2,每次从栈中弹出一个元素，搜索所有在它下一级的元素，把这些元素压入栈中
#       3,重复第2个步骤到结束为止

# 问题：
    # 无法检测出有问题的继承
    # 有可能还会违背”局部优先“的原则
    # 详情请查看 问题.png



# -------------------------------继承 - 资源的访问顺序 ---------------------------------
class C:
    age = "c"

class B(C):
    age = "b"

class A(B):
    age = "a"

p = A()
print(p.age)

# 单继承 查找的顺序：A -> B -> C


class E:
    age = "e"

class D:
    age = "d"

class C(E):
    age = "c"

class B(D):
    age = "b"

class A(B,C):
    # age = "a"
    pass

p = A()
print(p.age)  # b

# A -> B -> D -> C -> E


# 针对于几种标准原则的方案消化
# 经典类 -- 深度优先（从左到右）
# 新式类 -- 在深度有限（从左到右）的算法基础上，优化了一部分 -- 如果产生了重复元素，会保留最后一个
# 详情见 -- 多继承形态.png

class D(object):
    # age = "d"
    pass

class C(D):
    age = "c"

class B(D):
    age = "b"

class A(B,C):
    # age = "a"
    pass


print(A.age)  # b
print(inspect.getmro(A))
# 运行结果：
# <class '__main__.A'>,
# <class '__main__.B'>,
# <class '__main__.C'>,
# <class '__main__.D'>,
# <class 'object'>


# 广度优先 -- 沿着继承链，尽可能往宽的去找
# 具体算法步骤：
#   1，把根节点放到队列的末尾  -- 队列 -- 先进先出
#   2，每次从队列的头部取出一个元素，查看这个元素所有的下一级元素，把它们放到队列的末尾 ，发现已经被处理过，则略过
#   3，重复上面步骤


# -- 问题示例
# class D:
#     pass
#
# class B(D):
#     pass
#
# class C(B):
#     pass
#
# class A(B,C):
#     pass

# import inspect
# print("!"*100)
# print(inspect.getmro(A))


# -------------------------------- Python3 继承 -- 资源的访问顺序 -- 2.3- 2.7 C3算法  --------------------------------
# C3 算法
# 步骤：
#     真正步骤:
#       1. L(object) = [object]
#          L(子类（父类1， 父类2）) = [子类] + merge(L（父类1），L（父类2），[父类1，父类2])
#       2. + 代表合并列表
# 注意：
#       merge算法:
#           1, 第一个列表的第一个元素， 是后续列表的第一个元素  或者 后续列表中乜有再次出现，
#               则将这个元素合并到最终的解析列表中，并从当前刀座的所有列表中删除
#           2, 如果不符合，则跳过此元素，查找下一个列表的第一个元素，重复 1 的判断规则
#           3, 如果最终无法将所有元素归并到解析列表，则报错
#       类似拓扑算法，但并不是！ 切记


class D(object):
    pass
#   L(D) = [D] + merge(L(object),[object])
#        = [D] + merge([object], [object])
#        = [D, object] + merge([], [])
#        = [D, object]


class B(D):
    pass
#   L(B(D)) = [B] + merge(L(D),[D])
#           = [B] + merge(L[D,object],[D])
#           = [B,D] +merge([object],[])
#           = [B,D,object] + merge([],[])
#           = [B,D,object]


class C(D):
    pass
# L(C(D)) = [C,D,Object]


class A(B, C):
    pass
# L(A(B,C)) = [A] + merge(L(B),L(C),[B,C])
#           = [A] + merge([B,D,object],[C,D,object],[B,C])
#           = [A,B] + merge([D,object],[C,D,object],[C])
#           = [A,B,C] + merge([D,object],[D,object])
#           = [A,B,C,D] + merge([object],[object])
#           = [A,B,C,D,object] + merge([],[])
#           = [A,B,C,D,object]

import inspect
print(inspect.getmro(A))

# 继承查找方法：
#   1，根据代码的继承顺序，绘制出不同资源的结构图
#   2，找到入度为0 这样的一个节点
#   3，要把这个节点输出，并且，删除这个节点，然后，删除这个节点的出边，重新计算剩余节点的度数
#   4，重复以上步骤，得到资源访问顺序

# 资源的覆盖
#   包括： 属性的覆盖、 方法重写
#   原理：
#       1，在MRO的资源检索链中，
#       2，优先级比较高的类写了一个和优先级比较低的类一样的资源（属性 或 方法）
#       3，到时候，再去获取相关资源，就会优先选择优先级比较高的资源，
#           而摒弃优先级比较低的资源，造成“覆盖”的假象
#   注意：
#       1，只有优先级比较高的类，才能覆盖优先级比较低的类 中的资源，反过来不成立
#       2，当调佣优先级比较高的资源时，注意self、cls的变化

class D:
    # pass
    age = "d"

class C(D):
    # pass
    age = "c"
    def test(self):
        print("c")

class B(D):
    age = "b"
    def test(self):
        print("b")

    def test1(self):
        print(self)

    @classmethod
    def test2(cls):
        print(cls)

class A(B,C):
    pass

print(A.mro())
# (<class '__main__.A'>, <class '__main__.B'>, <class '__main__.C'>, <class '__main__.D'>, <class 'object'>)
print(A.age)
# b
print(A().test())  # b


# 当调佣优先级比较高的资源时，注意self、cls的变化
A.test2()
# <class '__main__.A'>  ---> 注意： 谁调用的这个方法，cls就指向谁

a = A()
print(a.test1())
# <__main__.A object at 0x000001A0DA41D2E0>  ---> 注意： 谁调用的这个方法，cls就指向谁



#  ----------------------------------  资源的累加 -----------------------------------------------------------
#   概念：在一个类的基础之上，增加一些额外的资源
#       子类相比于父类，多一些自己特有的资源
#       在被覆盖的方法基础之上，新增内容

class B:
    a = 1
    def __init__(self):
        self.b = 2

    def t1(self):
        print("t1")

    @classmethod
    def t2(cls):
        print("t2")

    @staticmethod
    def t3():
        print("t3")

class A(B):
    c = 3
    pass


a_obj = A()
print(A.a)  # 1
print(a_obj.b)  # 2

a_obj.t1()
A.t2()
A.t3()
# t1
# t2
# t3

print(A.c)  # 3

a_obj.d = "XXX"
print(a_obj.d)  # XXX


# -----  继承 -- 资源的累加2 -- __init__方法的累加 (即 通过类名来调用)-------------------------------
# 通过类名来调用
# 通过super() 来调用

class B:
    def __init__(self):
        self.b = 2
        self.xxx = "123"


class A(B):
    c = 3
    def __init__(self):
        self.e = "666"
        # 注意 ： 这里self == a_obj
        # self.init
        # 当使用self的方式来进行访问时，self.init依旧会先在A类中找到自己的__init__方法，形成死循环，不可能找到B类中去

        # b = B()
        # b.__init__()
        # 当使用在 A类中的__init__方法中在实例化B, 是可以访问的到的，但是不是给a的实例进行属性的累加

        # ----------------------------通过类名来调用------------------------------------
        B.__init__(self)   # 通过B类来调用父类的方法，但是不利于代码的维护

a = A()
print(a.__dict__)
# {'e': '666', 'b': 2, 'xxx': '123'}

# 想在需要实现的是通过A实例化的a_obj对象，来访问到 b 的属性
a_obj = A()

# 菱形关系，使用类来调用会重复调用
class D(object):
    def __init__(self):
        print("d")

class B(D):
    def __init__(self):
        D.__init__(self)
        print("b")

class C(D):
    def __init__(self):
        D.__init__(self)
        print("c")

class A(B,C):
    def __init__(self):
        B.__init__(self)
        C.__init__(self)
        print("a")


a = A()
print("!"*100)
print(a.__dict__)
# 运行结果：
# d
# b
# d
# c
# a



# -------------------------------通过super() 来累加类属性---------------------------------
# 在低优先级类的方法中，通过“super”调佣高优先级类的方法
#   概念： 是一个类，只在新式类中有效
#   作用： 起着代理的作用，帮助我们完成： 验证MRO（注意并不是父类，例子：菱形继承）链条，找到下一级节点，无调用对应的方法

#   问题： 沿着谁的MRO链条？ 找谁的下一个节点？ 如何应对类方法、静态方法以及实例方法的传参问题？
#   语法原理：
#       super(参数1, [,参数2])
#   工作原理：
#             def super(cls,inst):
#                 mro = inst.__class__mro()
#                 return mro[mro.index(cls) + 1]
#   问题解决：
#       沿着谁的MRO链条 --- 参数2
#       找谁的下一个节点 --- 参数1
#       如何应对类方法，静态方法以及实例方法的传参问题？ -- 使用参数2进行调用
#   常用具体语法形式：
#       python2.2+
#         super(type,obj) -> bound super object
#         super(type, type2) -> bound super object
#       python3+
#         super()

# 注意：
#   1，第一个参数，不要使用__class__ 方法来获取类对象
#   2，不要把super 调用，和铜鼓类名来调用 进行混合使用



# ---- 继承 - 资源的累加 -- super使用

class B:
    a = 1
    def __init__(self):
        self.b = 2
        self.xxx = "123"

    def t1(self):
        print("t1")

    @classmethod
    def t2(cls):
        print("t2")

    @staticmethod
    def t3():
        print("t3")


class A(B):
    c = 3

    def __init__(self):
        # super(A,self).__init__()
        # 或者
        super().__init__()  # 原理：找到方法所在的类 -- A 作为第一个参数，在找到所在方法的第一个参数 -- self 作为第二个参数
        self.e = "666"

    def tt1(self):
        print("t4")

    @classmethod
    def tt2(cls):
        # super(A, A).t2()  # 原理，这里第一个A表示的 验证A类mro链条，第二个A 因为tt2调用的是B类里面的类方法，所以应该在t2中传入一个类
        # 或者
        super(A, cls).t2()  # 这里的cls == <class '__mian'__.A >
        print("t5")

    @staticmethod
    def tt3():
        print("t6")

print("!"*100)
a = A()
print(a.__dict__)
# 运行结果：
# {'b': 2, 'xxx': '123', 'e': '666'}

A.tt2()
# 运行结果：
# t2
# t5


# ---- super 解决菱形继承的重复调用问题
# 菱形关系，使用类来调用会重复调用
class D(object):
    def __init__(self):
        print("d")

class B(D):
    def __init__(self):
        # D.__init__(self)
        super().__init__()
        print("b")

class C(D):
    def __init__(self):
        # D.__init__(self)
        # super(C, self).__init__()
        # 或者
        super().__init__()
        print("c")

class A(B,C):
    def __init__(self):
        # B.__init__(self)
        # C.__init__(self)
        super().__init__()
        print("a")


A()
# 运行结果：
# d
# c
# b
# a


# ------ 不要把super 调用，和铜鼓类名来调用 进行混合使用

class D(object):
    def __init__(self):
        print("d")

class B(D):
    def __init__(self):
        # D.__init__(self)
        super().__init__()
        print("b")

class C(D):
    def __init__(self):
        # D.__init__(self)
        # super(C, self).__init__()
        # 或者
        super().__init__()
        print("c")

class A(B,C):
    def __init__(self):
        B.__init__(self)  # 通过类名调用其他类的资源
        C.__init__(self)
        # super().__init__()
        print("a")


A()
# 运行结果：
# d
# c
# b
# d
# c
# a
print(A.mro())
# [<class '__main__.A'>, <class '__main__.B'>, <class '__main__.C'>, <class '__main__.D'>, <class 'object'>]