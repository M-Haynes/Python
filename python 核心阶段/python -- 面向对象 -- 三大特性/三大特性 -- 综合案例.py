# ----------------------------- 案例说明 -----------------------------------
# 定义三个类： 小狗、小猫、人
# 小狗：名字、年龄(默认1岁)；  吃饭、玩、睡觉、看家（格式： 名字是：xx， 年龄xx岁的小狗在xx）
# 小猫：名字、年龄(默认1岁)；  吃饭、玩、睡觉、捉老鼠（格式： 名字是：xx， 年龄xx岁的小猫在xx）
# 人：姓名，年龄(默认1岁)，宠物；  吃饭，玩，睡觉（格式： 名字是：xx， 年龄xx岁的小猫在xx）
#                             养宠物（让所有的宠物吃饭、玩、睡觉）
#                             让宠物工作（让所有的宠物根据自己的职责开始工作）

# --------------------------- 怎么通过代码 ，解决问题 -----------------------------
# 有开发经验：
#   问题 -> 根据自己的开发经验，制定出，一套自以为完美的解决方案 -> 通过方法实现这个方法 ->通过代码实现这个方法

# 无开发经验的：
#   有进取心的：
#      问题 -> 直接通过代码，实现能够想到的问题 -> 检测发现代码问题 -> 重构代码，不断的去升级方案
#   无进取心的：
#      问题 -> 直接通过代码，实现能够想到的问题

class Person(object):
    def __init__(self, name=None, pets = None, age=1):
        self.name = name
        self.age = age
        self.pets = pets

    def eat(self):
        print("%s在吃" % self)

    def play(self):
        print("%s在玩" % self)

    def sleep(self):
        print("%s在睡觉" % self)

    def feed_pet(self):
        # （让所有的宠物吃饭、玩、睡觉）
        for pet in self.pets:
            pet.eat()
            pet.play()
            pet.sleep()

    def make_pets_work(self):
        for pet in self.pets:
            # if isinstance(pet, Dog):
            #     pet.watch()
            # elif isinstance(pet, Cat):
            #     pet.catch()

            # 优化2
            pet.work()

    def __str__(self):
        return "名字是：{}，年龄是：{}的人".format(self.name, self.age)


class Cat(object):
    def __init__(self, name=None, age=1):
        self.name = name
        self.age = age

    def eat(self):
        print("%s在吃" % self)

    def play(self):
        print("%s在玩" % self)

    def sleep(self):
        print("%s在睡觉" % self)

    # def catch(self):
    #     print("%s在捉老鼠" % self)

    # 优化2
    def work(self):
        print("%s在捉老鼠" % self)

    def __str__(self):  # 表示对self这个参数进行一个字符串的设置
        return "名字是：{}， 年龄{}岁的小猫".format(self.name, self.age)


class Dog(object):
    def __init__(self, name=None, age=1):
        self.name = name
        self.age = age

    def eat(self):
        # print("名字是：{}，年龄{}岁的小狗在吃饭".format(self.name, self.age))
        # 改进1
        print("%s在吃" % self)

    def play(self):
        # print("名字是：{}，年龄{}岁的小狗在玩".format(self.name, self.age))
        # 改进1
        print("%s在玩" % self)

    def sleep(self):
        # print("名字是：{}， 年龄{}岁的小狗在睡觉".format(self.name, self.age))
        # 改进1
        print("%s在睡觉" % self)

    # def watch(self):
    #     print("名字是：xx， 年龄xx岁的小狗在砍价".format(self.name, self.age))
    #     改进1
        # print("%s在看家" % self)

    # 优化2
    def work(self):
        print("%s在看家" % self)

    # 改进1
    def __str__(self):  # 表示对self这个参数进行一个字符串的设置
        return "名字是：{}， 年龄{}岁的小狗".format(self.name, self.age)

d = Dog("小黑")
# d.eat()
# 运行结果：
# 名字是：小黑，年龄1岁的小狗在吃饭

# 改进1的运行
# d = Dog("小白")
# d.sleep()
# 运行结果：
# 名字是：小白， 年龄1岁的小狗在睡觉

c = Cat("小红", 2)
p = Person("sz", [d, c], age=18)

# p.feed_pet()
# 运行结果：
# 名字是：小黑， 年龄1岁的小狗在吃
# 名字是：小黑， 年龄1岁的小狗在玩
# 名字是：小黑， 年龄1岁的小狗在睡觉
# 名字是：小红， 年龄2岁的小猫在吃
# 名字是：小红， 年龄2岁的小猫在玩
# 名字是：小红， 年龄2岁的小猫在睡觉

p.make_pets_work()
# 运行结果：
# 名字是：小黑， 年龄1岁的小狗在看家
# 名字是：小红， 年龄2岁的小猫在捉老鼠


# ---------------------------------------- 使用面向对象 -- 继承 进行优化------------------------
# 设置父类
class Animal:
    def __init__(self, name, age=1):
        self.name = name
        self.age = age

    def eat(self):
        print("%s在吃饭" % self)

    def play(self):
        print("%s在玩" % self)

    def sleep(self):
        print("%s在睡觉" % self)


class Person(Animal):
    def __init__(self, name, pets, age):  # 注意： self == Person实例
        # 使用super调用Animal类中的__init__中的方法，实现资源的累加
        super(Person, self).__init__(name, age)
        self.pets = pets
    def feed_pet(self):
        # （让所有的宠物吃饭、玩、睡觉）
        for pet in self.pets:
            pet.eat()
            pet.play()
            pet.sleep()

    def make_pets_work(self):
        for pet in self.pets:
            pet.work()

    def __str__(self):
        return "名字是{}，年龄{}岁的人".format(self.name, self.age)

# p = Person("ax", [1,2],18)
# print(p.__dict__)

class Cat(Animal):

    def work(self):
        print("%s在捉老鼠" % self)

    def __str__(self):
        return "名字是{}，年龄{}岁的小猫".format(self.name, self.age)


class Dog(Animal):

    def work(self):
        print("%s在看家" % self)

    def __str__(self):
        return "名字是{}，年龄{}岁的小狗".format(self.name, self.age)


d = Dog("小白")
c = Cat("小红", 2)
p = Person("sz", [d, c], age=18)
# p.feed_pet()
p.make_pets_work()


